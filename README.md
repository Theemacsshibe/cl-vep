# cl-vep is a Common Lisp Video Effects Processor

cl-vep does visual effects very damn well. It doesn't use a job or project
format, rather it provides many functions for manipulating video frames,
readers, writers and masks which are available at a standard Lisp REPL and
can be sequenced and combined using standard Lisp forms. cl-vep can handle
multiple cores with a frame-level concurrency model, and will soon target
the GPU through [oclcl](https://github.com/gos-k/oclcl) -- if I ever update
it, hah.

## dependencies

You will need:

- `external-program`, `opticl` and `bordeaux-threads`, all loadable via the
  fantastic Quicklisp system manager
- ffmpeg for decoding and encoding videos

## bugs

- parallel-mapvideo may blow the heap if you crash it too many times. Threads
  seemingly aren't always stopped, but occurences of this are very random.
- The parallel-mapvideo buffer always caps a fixed amount of frames, not
  factoring in the sizes of frames. More smaller frames can fit in the same
  space as less larger frames.
- The ffmpeg pipe interface only seems to get to 6fps maximum, usually around
  2 to 4fps for most loads. This is drastically slower than most jobs, with
  parallel-mapvideo only able to create a load of 1.5 on my 12 thread computer.

## what, lisp?

You heard me right. Common Lisp with enough declarations is pretty damn fast,
and also comes with free garbage collection and works wonders for domain-
specific languages. For example, most CPU shaders rely on `process-image` and
`process-mask` which create a mask and set each pixel to a value provided by
the programmer. As for speed, SBCL complains it can't take the reciprocal of
3.0 accurately, so it probably has a lot of tricks.
