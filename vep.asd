(defsystem vep
    :depends-on (external-program opticl bordeaux-threads trivial-garbage)
    :components ((:module outside-world
			  :components ((:file "package")
				       (:file "ffmpeg" :depends-on ("package"))
				       (:file "image" :depends-on ("package"))))
		 (:module cpu
			  :components ((:file "package")
				       (:file "utilities" :depends-on ("package"))
                                       (:file "masks" :depends-on ("package"))
				       (:file "convolution" :depends-on ("package"
									 "utilities"))
                                       (:file "blur" :depends-on ("package""utilities"))))
                 (:file "package")
                 (:module higher-order
                          :components ((:file "greenscreen")
                                       (:file "edges")
                                       (:file "do-full-video")))))
