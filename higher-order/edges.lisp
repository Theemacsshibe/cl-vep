(in-package :vep)

(defun 4-edge-detector ()
  (convolution-3x3-matrix
   (make-array '(3 3)
               :element-type 'single-float
               :initial-contents '((0.0  1.0 0.0)
                                   (1.0 -4.0 1.0)
                                   (0.0  1.0 0.0)))))

(defun 8-edge-detector ()
  (convolution-3x3-matrix
   #2a((-1.0 -1.0 -1.0)
       (-1.0 8.0  -1.0)
       (-1.0 -1.0 -1.0))))
