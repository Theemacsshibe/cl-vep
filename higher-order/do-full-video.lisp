(in-package :vep)

(defvar *buffer-size* 25)
(defvar *buffer-pixel-size* (expt 2 25))

(defun mapvideo (function output-name fps &rest videos)
  (let ((writer (ffmpeg-write-generator output-name
                                        :fps fps)))
    (catch 'end-of-file
      (handler-case 
          (loop
             for frames = (mapcar #'funcall videos)
             do (funcall writer (apply function frames)))
        (end-of-file (e)
          (declare (ignore e))
          (throw 'end-of-file nil))
        (condition (e)
          (error e))))
    (funcall writer :finished)))

(defun parallel-mapvideo (threads function output-name fps &rest videos)
  (let ((results (make-hash-table))
        (generators-lock (make-lock "Generator lock"))
        (frame-number 0)
        (buffer-size *buffer-size*)
        (collector-frame-number 1)
        (running-n threads)
        (writer (ffmpeg-write-generator output-name
                                        :fps fps)))
    (dotimes (n threads)
      (make-thread
       (lambda ()
         (catch 'end-of-file
           (handler-case
               (loop
                  (loop while (> (- frame-number collector-frame-number)
                                 buffer-size)
                     do (sleep 0.05))
                  (let (frames current-frame result)
                    (with-lock-held (generators-lock)
                      (setf frames (mapcar #'funcall videos)
                            current-frame (1+ frame-number)
                            frame-number current-frame))
                    (format t "Working on frame ~d.~%" current-frame)
                    (setf result (apply function frames))
                    (with-lock-held (generators-lock)
                      (setf (gethash current-frame results)
                            result))))
             (end-of-file (e)
               (declare (ignore e))
               (throw 'end-of-file nil))
             (condition (c)
               (error c))))
         (with-lock-held (generators-lock)
           (decf running-n)))))
    (handler-case
        (loop
           until (and (zerop running-n)
                      (zerop (hash-table-count results)))
           do (multiple-value-bind (value win)
                  (gethash collector-frame-number results)
                (if win
                    (progn
                      (format t "Writing frame ~d.~%" collector-frame-number)
                      (funcall writer value)
                      (remhash collector-frame-number results)
                      (when (zerop (mod collector-frame-number 25))
                        (destructuring-bind (height width depth)
                            (array-dimensions value)
                          (declare (ignore depth))
                          (let ((new-size (round
                                           (/ *buffer-pixel-size*
                                              (* height width)))))
                            (format t "Adjusted buffer-size to ~d.~%" new-size)
                            (setf buffer-size new-size))))
                      (incf collector-frame-number))
                    (sleep 0.02))))
      (condition (c)
        (setf frame-number :foo)
        (error c)))
    (gc :full t)
    (funcall writer :finished)))
