(in-package :vep)

(defun green-screen (original-image &key (diff-start 0.05) (diff-end 0.075) (blendfn #'identity))
  (let ((mask (image-diff-mask original-image
                               :diff-start diff-start
                               :diff-end diff-end)))
    (lambda (current-image replacement-image)
      (let ((blender (mask-blend (funcall mask current-image) blendfn)))
        (funcall blender current-image replacement-image)))))

