(in-package :vep.outside-world)

(declaim (optimize (speed 3)
                   (safety 1))
         (ftype (function (single-float)
                          (unsigned-byte 8))
                float->u8))

(defun float->u8 (f)
  (declare (single-float f))
  (the (unsigned-byte 8)
       (values
        (truncate (* f 255.0)))))

(defun normalise-image (image)
  (declare (optimize (speed 3))
           ((simple-array (unsigned-byte 8) (* * *)) image))
  (destructuring-bind (height width depth)
      (array-dimensions image)
    (let ((new-image (make-array (array-dimensions image)
                                 :element-type 'single-float
                                 :initial-element 0.0)))
      (declare (fixnum height width depth))
      (dotimes (x width)
        (dotimes (y height)
          (dotimes (c depth)
            (setf (aref new-image y x c)
                  (/ (aref image y x c) 256.0)))))
      new-image)))

(defun quantize-image (image)
  (declare (optimize (speed 3))
           ((simple-array single-float (* * *)) image))
  (destructuring-bind (height width depth)
      (array-dimensions image)
    (declare (fixnum height width depth))
    (let ((new-image (make-array (array-dimensions image)
                                 :element-type '(unsigned-byte 8)
                                 :initial-element 0)))
      (dotimes (x width)
        (dotimes (y height)
          (dotimes (c depth)
            (setf (aref new-image y x c)
                  (min 255
                       (max 0
                            (float->u8 (aref image y x c))))))))
      new-image)))

(defun write-image (image filename)
  (write-image-file filename
                    (quantize-image image)))

(defun read-image (filename)
  (normalise-image
   (read-image-file filename)))
