(in-package :vep.outside-world)

(defvar *ffmpeg-path* "ffmpeg")

(defun ffmpeg-read-generator (pathname)
  (let* ((process (start *ffmpeg-path*
                         `("-i" ,pathname
                           "-f" "image2pipe"
                           "-vcodec" "ppm"
                           "-loglevel" "quiet"
                           "-")
                         :error t
                         :output :stream))
         (process-output (process-output-stream process)))
    (lambda (&optional message)
      (case message
        (:finished (close process-output))
        ((nil)
         (loop with win = nil
            until win
            do (handler-case
                   (setf win
                         (normalise-image
                          (read-ppm-stream process-output)))
                 (simple-error (e)
                   (declare (ignore e))
                   (if (null
                        (peek-char nil process-output nil nil))
                       (error 'end-of-file)
                       (sleep 0.05)))
                 (condition (e)
                   (error e)))
            finally (return win)))))))

(defun ffmpeg-write-generator (pathname &key
                                          (fps 30)
                                          (preset "ultrafast"))
  ;; -f image2pipe -framerate 1 -i - -c:v libx264 -vf format=yuv420p -r 25 out.mp4
  (let* ((process (start *ffmpeg-path*
                         `("-f" "image2pipe"
                           "-framerate" ,fps
                           "-vcodec" "ppm"     
                           "-i" "-"
                           "-c:v" "libx264"
                           "-preset" ,preset
                           "-vf" "format=yuv420p"
                           "-r" ,fps
                           "-y" ,pathname)
                         :error t
                         :input :stream))
         (process-input (process-input-stream process)))
    (lambda (message)
      (if (eql message :finished)
          (close process-input)
          (progn
            (write-ppm-stream
             process-input
             (quantize-image message)))))))
