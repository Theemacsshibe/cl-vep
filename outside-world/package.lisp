(defpackage :vep.outside-world
  (:use :cl :opticl :external-program)
  (:export :ffmpeg-read-generator
           :ffmpeg-write-generator
           :write-image
           :read-image))
