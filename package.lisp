;; Be more aggresive when GCing.
#+sbcl  (setf (sb-ext:bytes-consed-between-gcs) (expt 2 26))
#+cmucl (setf ext:*bytes-consed-between-gcs* (expt 2 26))

(defpackage :vep
  (:use :cl
        :bordeaux-threads
        :trivial-garbage
	:vep.outside-world
	:vep.cpu)
  (:export :parallel-mapvideo
           :mapvideo
           :4-edge-detector
           :8-edge-detector
           :green-screen))

(defpackage :vep-user
  (:use :cl
        :vep
        :vep.cpu
        :vep.outside-world))
