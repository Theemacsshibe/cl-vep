(defpackage :vep.cpu
  (:use :cl)
  (:export :convolution-3x3-matrix
           :sine-blender
           :image-diff-mask
           :box-blur
           :fake-gaussian-blur
           :mask-blend))
