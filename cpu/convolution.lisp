(in-package :vep.cpu)

(declaim (optimize (speed 3)))

(defmacro valid-or-zero (image y x c)
  `(if (and (< -1 ,x width)
            (< -1 ,y height))
       (aref ,image ,y ,x ,c)
       0.0))

(defmacro convolution-multiply (scale image offset-x offset-y)
  `(* ,scale (valid-or-zero ,image
                          (+ y ,offset-y)
                          (+ x ,offset-x)
                          c)))

(defun convolution-3x3-matrix (matrix &optional (scale 1.0))
  (declare ((simple-array single-float (3 3)) matrix)
           (single-float scale))
  (let ((tl (aref matrix 0 0))
        (tm (aref matrix 1 0))
        (tr (aref matrix 2 0))
        (ml (aref matrix 0 1))
        (mm (aref matrix 1 1))
        (mr (aref matrix 2 1))
        (bl (aref matrix 0 2))
        (bm (aref matrix 1 2))
        (br (aref matrix 2 2)))
    (lambda (image)
      (declare ((simple-array single-float (* * 3)) image))
      (process-image image
        (*
         (+
          (convolution-multiply tl image -1 -1)
          (convolution-multiply tm image 0  -1)
          (convolution-multiply tr image 1  -1)
          (convolution-multiply ml image -1 0)
          (convolution-multiply mm image 0  0)
          (convolution-multiply mr image 1  0)
          (convolution-multiply bl image -1 1)
          (convolution-multiply bm image 0  1)
          (convolution-multiply br image 1  1))
         scale)))))
