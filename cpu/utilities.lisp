(in-package :vep.cpu)

(defmacro process-image (old-image &body expr)
  `(let* ((size (array-dimensions ,old-image))
          (new-image (make-array size
                                  :element-type 'single-float
                                  :initial-element 0.0)))
     (destructuring-bind (height width depth) size
       (declare (fixnum height width depth))
       (dotimes (x width)
         (dotimes (y height)
           (dotimes (c depth)
             (declare (fixnum x y c))
             (setf (aref new-image y x c)
                   (progn ,@expr))))))
     new-image))

(defmacro process-mask (old-image &body expr)
  (let ((unused-depth (gensym "UNUSED-DEPTH")))
    `(destructuring-bind (height width ,unused-depth)
         (array-dimensions ,old-image)
       (declare (ignore ,unused-depth))
       (let ((new-mask (make-array (list height width)
                                   :element-type 'single-float
                                   :initial-element 0.0)))
         (declare (fixnum height width))
         (dotimes (x width)
           (dotimes (y height)
             (declare (fixnum x y))
             (setf (aref new-mask y x)
                   (progn ,@expr))))
         new-mask))))

(defmacro let-1 ((variable binding) &body body)
  `(let ((,variable ,binding))
     ,@body))
