(in-package :vep.cpu)

(declaim (optimize (speed 3) (safety 1)))

(defun box-blur (blur-size)
  (declare (fixnum blur-size))
  (lambda (image)
    (declare ((simple-array single-float (* * 3)) image))
    ;; Using two passes makes blurring O(2n*size) instead of O(size*n^2).
    ;; Accumulating blurs makes it O(n+size).
    (let* ((size (array-dimensions image))
           (horizontals (make-array size
                                    :element-type 'single-float
                                    :initial-element 0.0))
           (blurred (make-array size
                                :element-type 'single-float
                                :initial-element 0.0)))
      (destructuring-bind (height width depth)
          size
        ;; Pass 1: blur horizontally.
        (dotimes (y height)
          (dotimes (c depth)
            (let ((accumulator 0.0)
                  (left-most   0.0)
                  (last-left-x   0)
                  (last-right-x  0))
              (dotimes (x width)
                (declare (fixnum y c x)
                         (single-float accumulator))
                ;; Don't count out-of-bounds positions.
                (let* ((left-x  (max (- x blur-size) 0))
                       (right-x (min (+ x blur-size) (1- width)))
                       (real-size (1+ (- right-x left-x))))
                  (cond
                    ((zerop x)
                     (setf accumulator
                           (loop
                              for this-x of-type fixnum
                              from left-x below right-x
                              summing (aref image y this-x c))))
                    (t
                     (when (/= left-x  last-left-x)
                       (decf accumulator left-most))
                     (when (/= right-x last-right-x)
                       (incf accumulator (aref image y right-x c)))))
                  (setf (aref horizontals y x c)
                        (/ accumulator real-size)
                        left-most (aref image y left-x c)
                        last-left-x  left-x
                        last-right-x right-x)
                  (when (or (< accumulator 0.0)
                            (> accumulator real-size))
                    (error "Accumulator overflow!~%acc=~f,size=~d @ (~d,~d,~d)"
                            accumulator real-size x y c)))))))
        ;; Pass 2: blur vertically.
        (dotimes (y height)
          (dotimes (x width)
            (dotimes (c depth)
              (declare (fixnum x y c))
              ;; Also don't count out-of-bounds positions
              (let* ((top-y    (max (- y blur-size) 0))
                     (bottom-y (min (+ y blur-size) (1- height)))
                     (real-size (- bottom-y top-y)))
                (dotimes (c depth)
                  (setf (aref blurred y x c)
                        (loop
                           for this-y from top-y to bottom-y
                           summing (aref horizontals this-y x c) into sum
                           finally (return (/ sum real-size))))))))))
      ;; Voila, you have a blurred image.
      blurred)))

(defun fake-gaussian-blur (blur-size &optional (blur-steps 3))
  (declare (fixnum blur-size blur-steps))
  (let ((size-per-step (floor blur-size blur-steps)))
    (declare (fixnum size-per-step))
    (lambda (image)
      (declare ((simple-array single-float (* * 3)) image))
      (loop
         with step = image
         with blurrer = (box-blur size-per-step)
         for n of-type fixnum below blur-steps
         do (setf step (funcall blurrer step))
         finally (return step)))))
