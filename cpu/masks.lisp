(in-package :vep.cpu)

(declaim (inline distance)
         (optimize (speed 3)
                   (safety 1)))

(defun distance (a b)
  (declare (type single-float a b))
  (the single-float (abs (- a b))))

(defun sine-blender (x)
  (declare (type single-float x))
  (coerce (sin (* pi (- x 0.5))) 'single-float))

(defun image-diff-mask (base &key
                               (diff-start 0.1)
                               (diff-end 0.15))
  (declare (single-float diff-start diff-end)
           (type (simple-array single-float (* * 3)) base))
  (let ((diff-size (- diff-end diff-start)))
    (declare (single-float diff-size))
    (assert (> diff-size 0) (diff-size)
            "difference range must be greater than zero")
    (lambda (image)
      (declare (type (simple-array single-float (* * 3)) image))
      (assert (equal (array-dimensions image)
                     (array-dimensions base))
              (image base) "image dimensions are not equal")
      (process-mask image
        (let ((difference (/ (+ (distance (aref base  y x 0)
                                          (aref image y x 0))
                                (distance (aref base  y x 1)
                                          (aref image y x 1))
                                (distance (aref base  y x 2)
                                          (aref image y x 2)))
                             3.0)))
          (cond
            ((< difference diff-start) 0.0)
            ((> difference diff-end)   1.0)
            (t (/ (- difference diff-start)
                  diff-size))))))))

(defun mask-blend (mask &optional (blender #'identity))
  (declare ((simple-array single-float (* *)) mask))
  (lambda (image1 image2)
    (declare ((simple-array single-float (* * 3)) image1 image2))
    (assert (equal (array-dimensions image1)
                   (array-dimensions image2))
            (image1 image2) "image dimensions are not equal")
    (let ((new-image (make-array (array-dimensions image1)
                                 :element-type 'single-float
                                 :initial-element 0.0)))
      (destructuring-bind (height width depth)
          (array-dimensions image1)
        (dotimes (x width)
          (dotimes (y height)
            (let* ((mix (funcall blender (aref mask y x)))
                   (inv-mix (- 1.0 mix)))
              (declare (single-float mix inv-mix))
              (dotimes (c depth)
                (setf (aref new-image y x c)
                      (+ (* mix     (aref image1 y x c))
                         (* inv-mix (aref image2 y x c)))))))))
      new-image)))
