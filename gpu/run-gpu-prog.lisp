(in-package :vep-gpu)


(defmacro make-pixel-shader (name args &body shader)
  `(defkernel ,name (void ,@args)
     (let* ((
